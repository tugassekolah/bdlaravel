-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2020 at 05:16 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(3, 'mantap Djiwa', 'suatu kata yang sering di ucapkan', NULL, NULL, NULL, 'Kata sehari-hari'),
(4, 'oawkoakwo', 'oakwowakokwoa', NULL, NULL, NULL, 'aokwoawk'),
(5, 'Mrs.', 'Quo corporis mollitia quas tempore. Et iste totam possimus quibusdam voluptatem qui facere. Laudantium tenetur sit sed.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Proofreaders and Copy Marker'),
(6, 'Prof.', 'Magni enim dolor dignissimos fugit earum enim consequatur. Cumque quasi deserunt sit cum. Sit blanditiis officia id quo qui.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Textile Machine Operator'),
(7, 'Mrs.', 'Doloribus aspernatur quo recusandae commodi error maiores sunt. Nesciunt et dolores commodi in necessitatibus sint quis. Aut consectetur voluptatem ut similique dolor explicabo sapiente. Quis iure occaecati dolore debitis. Consequatur et perspiciatis atque omnis dolorem accusantium est.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Social and Human Service Assistant'),
(8, 'Mr.', 'Porro quidem ducimus dicta sit. Harum similique consequatur harum non. Dicta tenetur dolores ut qui magnam distinctio nihil suscipit. In expedita id distinctio sed a alias.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Materials Scientist'),
(9, 'Ms.', 'Dolorem quam quo harum eligendi. Provident quisquam omnis dolor ut. Commodi ipsa omnis error soluta id sunt.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Rental Clerk'),
(10, 'Ms.', 'Fugiat perferendis aut voluptas dignissimos labore ex est. Natus minima itaque in. Quo maxime eos nesciunt impedit aut.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Fence Erector'),
(11, 'Miss', 'Autem ut corporis quis. Odit quam velit temporibus quod sequi. Odit voluptatem repudiandae quis accusamus voluptas cum dignissimos. Mollitia soluta voluptatibus sed quo quibusdam.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Editor'),
(12, 'Prof.', 'Autem quo commodi est autem occaecati maiores velit nulla. Aspernatur est ab eos quia. Error qui dignissimos dignissimos molestiae minima sed. Id numquam debitis quos maiores similique qui adipisci unde.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Milling Machine Operator'),
(13, 'Prof.', 'Ipsam id ipsam consequatur amet quisquam. Voluptatem voluptates minima dolorum cupiditate perspiciatis soluta facere quia. Dolor impedit voluptatum recusandae quam dolore ex.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Reservation Agent OR Transportation Ticket Agent'),
(14, 'Dr.', 'Sit ut quo dolores ex illo. Accusantium ut commodi dignissimos magni blanditiis sunt enim. Laboriosam natus nihil earum facere qui. Voluptatem voluptates et eaque dolorem laborum ut.', '2020-08-26 20:17:32', '2020-08-26 20:17:32', NULL, 'Ship Carpenter and Joiner'),
(15, 'Mrs.', 'Iusto est assumenda accusamus eos quis qui iusto. Molestiae voluptatem quidem amet similique velit quod. Impedit pariatur temporibus omnis. Harum at quia consectetur ex iste ut labore.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Precision Pattern and Die Caster'),
(16, 'Mr.', 'Doloremque esse est enim officia assumenda animi. Et ea quae fugit pariatur. Voluptatem fugit commodi qui tenetur adipisci consequatur. Reprehenderit laboriosam non odit ea dolores.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Animal Trainer'),
(17, 'Miss', 'Dolorum dicta alias et. Qui officia omnis consequuntur ea esse expedita et. Numquam impedit tenetur eveniet reprehenderit et assumenda atque facere. Amet et ratione voluptas unde voluptates.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Fiber Product Cutting Machine Operator'),
(18, 'Mrs.', 'Molestiae omnis eum odio accusantium error. Ipsum labore dicta optio. Ut similique consequatur id harum dignissimos ut quis. Unde consequatur hic voluptatem ab nihil error.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Food Tobacco Roasting'),
(19, 'Prof.', 'Sed voluptates accusamus aspernatur dignissimos odio molestias repellat. Rerum odio id commodi rem. Provident quasi facere perferendis totam. Rerum aut suscipit laborum.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Aircraft Launch and Recovery Officer'),
(20, 'Miss', 'Sunt reprehenderit earum voluptatem suscipit iure id qui. Officia facere eligendi sit eum.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Agricultural Technician'),
(21, 'Miss', 'Ea praesentium saepe est. Sapiente repudiandae omnis sint blanditiis. Veritatis qui et veniam dolorem et eaque quis.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Geographer'),
(22, 'Ms.', 'Quia sint aut reiciendis vel nesciunt fugit. Atque dolore ut expedita. Placeat sed ut ducimus eius.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Aircraft Structure Assemblers'),
(23, 'Mr.', 'Velit ratione doloremque quas aliquam aut reiciendis. Porro excepturi dicta aut. Sunt enim aut eum est quibusdam esse est. Numquam quasi aut dolorem qui totam.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Automatic Teller Machine Servicer'),
(24, 'Mr.', 'Earum possimus est soluta amet totam ratione voluptatem voluptas. Quod rerum tenetur ipsum nihil tenetur reprehenderit quis. Dolor est et itaque enim voluptatem natus sit.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Woodworking Machine Operator'),
(25, 'Dr.', 'Distinctio ipsum et et iste minima molestias amet. Quis asperiores commodi ut. Fuga maxime tempora iure odit dolorem qui. Ea sint et est ut.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Copy Machine Operator'),
(26, 'Dr.', 'Et earum eius nobis quae repudiandae omnis. Quia aspernatur ut tenetur aut officiis. Architecto aut sunt tempora. Officia rerum omnis et impedit voluptate rem laudantium unde. Facilis illum excepturi porro soluta voluptatem.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Railroad Inspector'),
(27, 'Miss', 'Nisi eaque qui tempore qui est. Corrupti aut dicta aperiam occaecati ducimus nostrum commodi quis. Quae molestias vitae recusandae.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Hand Sewer'),
(28, 'Mrs.', 'Laboriosam excepturi aut assumenda error optio quae. Eum voluptatem non itaque voluptatem dolore quis. Autem voluptatum voluptatum aut aut ducimus at. A aperiam voluptates nisi quasi laborum. Eius vitae qui architecto excepturi hic.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Insurance Appraiser'),
(29, 'Miss', 'Voluptatem dignissimos consectetur minus quia ut nostrum. Nisi sint sint fugit labore minima. Debitis atque sit provident deleniti magni ut quod.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Human Resources Specialist'),
(30, 'Prof.', 'Voluptas unde sapiente autem quis modi et. Aspernatur est optio quam. Consectetur et quam dolores fuga quaerat magnam corporis.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Coroner'),
(31, 'Prof.', 'Deserunt architecto praesentium explicabo quia quidem voluptas magni doloremque. Eaque fugit alias rerum sed cumque. Quis et accusamus consequuntur assumenda aut voluptatibus doloremque. In repellendus impedit numquam molestias minima mollitia rerum.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Entertainment Attendant'),
(32, 'Prof.', 'Rerum molestias illum repellendus. Consequatur dolores culpa officiis et doloribus. Et reprehenderit qui non maiores exercitationem provident alias sit.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Sociology Teacher'),
(33, 'Dr.', 'Possimus praesentium sit architecto delectus labore occaecati. Corrupti et ratione cupiditate. Omnis qui ut sit sint officiis quis et.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Retail Salesperson'),
(34, 'Mr.', 'Odio id dolores enim reprehenderit beatae. Qui eligendi culpa ea autem commodi occaecati quasi ipsa. Cum amet aut assumenda. Ullam impedit quos illum voluptatem architecto magnam.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Equal Opportunity Representative'),
(35, 'Dr.', 'Sit assumenda consectetur eveniet consectetur. Dolorem tempore dolores blanditiis. Dicta odio laboriosam doloremque quo. Assumenda non corrupti illum mollitia.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Plasterer OR Stucco Mason'),
(36, 'Prof.', 'Est aut doloribus qui rerum labore illum nostrum. Qui animi ducimus expedita facere optio et. Perspiciatis eos et repellendus minima sed saepe.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Market Research Analyst'),
(37, 'Dr.', 'Rerum debitis sint non. Quis perspiciatis eum quo eos modi rerum veritatis. Consequuntur sit perferendis non perferendis.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Claims Adjuster'),
(38, 'Prof.', 'Iure eius libero necessitatibus cupiditate ut quo qui voluptatem. Repellendus perferendis iste architecto officia minima quo. Rerum placeat est maxime harum et voluptatem enim. Occaecati et exercitationem iusto.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Petroleum Engineer'),
(39, 'Ms.', 'Illum quibusdam sunt laborum molestias animi ea eum. Quidem expedita velit facere nobis. Dolorem provident esse occaecati enim. Assumenda ipsam voluptas quia est nihil nesciunt.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Rail Transportation Worker'),
(40, 'Ms.', 'Sed velit ullam sit minima sint fugit est. Excepturi et facilis sit inventore porro expedita. Et placeat voluptatem atque delectus et.', '2020-08-26 20:17:33', '2020-08-26 20:17:33', NULL, 'Government Service Executive'),
(41, 'Prof.', 'Facere eos cupiditate sunt. At consequuntur commodi voluptas soluta. Dolore aut quaerat maiores corporis. Rem corrupti labore deserunt illum quisquam repellat dolor.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Gas Processing Plant Operator'),
(42, 'Mr.', 'Deserunt doloribus quis optio perspiciatis. Qui odio suscipit corrupti quia atque. Porro quibusdam quia sed possimus. Vero error et corporis aut rerum in ea.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Event Planner'),
(43, 'Miss', 'Expedita enim incidunt aspernatur sunt itaque quae qui iusto. Perferendis aut soluta dicta delectus quaerat. Quasi repellendus hic rerum. Quidem non quidem aut vero sapiente. Vero corrupti tenetur adipisci eaque rerum ducimus.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Locomotive Engineer'),
(44, 'Dr.', 'Explicabo doloremque dolores velit doloremque aut laborum amet. Facere id placeat repellendus eos deleniti. Eaque laboriosam numquam eum culpa. Quo sint repudiandae architecto et quis possimus id.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Psychiatrist'),
(45, 'Dr.', 'Aperiam nostrum eius deleniti et beatae rem excepturi. Omnis eum ab et. Sunt odit quo eum aut.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Boilermaker'),
(46, 'Dr.', 'Odit fugiat unde aut vero est exercitationem est. Odio nemo qui quia praesentium aut culpa qui quo. Quaerat nesciunt culpa ut est aut.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Webmaster'),
(47, 'Prof.', 'Doloremque qui atque non qui. Quidem quisquam recusandae dolor officiis. Pariatur magni atque enim maiores officiis et.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Statement Clerk'),
(48, 'Mrs.', 'Nostrum incidunt voluptatibus ullam et. Aspernatur non reiciendis itaque illo illum. Qui esse ullam aut porro officiis iste cupiditate.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Credit Checker'),
(49, 'Dr.', 'Possimus consequatur sit natus. Ut enim quia aut provident voluptatem eum reiciendis.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Forest and Conservation Technician'),
(50, 'Prof.', 'Officiis at nam et omnis magnam corporis sed. Eos repudiandae perferendis nesciunt quis id. Quae occaecati unde est suscipit repellendus dolorum.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Plant Scientist'),
(51, 'Mrs.', 'Mollitia dignissimos repudiandae amet explicabo labore commodi. Qui ducimus eaque porro voluptatum dolorem quaerat quia. Eius occaecati fugiat quia quis ut quidem. Fugiat corporis minima voluptatem dolorem esse iusto delectus iusto.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Radiologic Technician'),
(52, 'Mr.', 'Ea nihil vitae eos consequatur qui exercitationem amet quo. Enim neque animi nemo minima explicabo. Minus molestias et ut error laboriosam.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Molding Machine Operator'),
(53, 'Dr.', 'Aut expedita consequatur et nesciunt ut fuga. Adipisci voluptas est natus voluptate qui ullam suscipit quisquam. Maiores nam eveniet error unde.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Animal Control Worker'),
(54, 'Mrs.', 'Quo ut quia aut minus tenetur eius. Dolor veritatis voluptatem voluptas voluptatem iste. Accusantium quas aut nisi voluptatem magni est. Ea consequatur occaecati modi et qui. Nihil sed et minus maxime.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Economist'),
(55, 'Prof.', 'Dolorem quasi a maiores omnis debitis. Non est hic ut dolor cumque omnis. Culpa et nesciunt nostrum et quis ipsam dolor sed. Est voluptatem assumenda dolores non dolor placeat.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'School Bus Driver'),
(56, 'Dr.', 'Ad quo tempora voluptatibus. Ea voluptatem voluptatem eveniet labore neque. Repellendus dolorum quis nisi omnis fugit dolorem impedit. Exercitationem mollitia magnam cumque pariatur ut. Sed modi blanditiis vel.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Model Maker'),
(57, 'Dr.', 'Consequatur dolor pariatur quaerat aut nesciunt. Consequatur qui ut exercitationem dolorem expedita quia. Error dolorem dolores vel amet dolor.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Stationary Engineer'),
(58, 'Dr.', 'Hic dignissimos aliquid nisi alias iste quidem voluptas. Enim laudantium consequatur explicabo illo. Magni cupiditate consequuntur est voluptate quis delectus.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Emergency Management Specialist'),
(59, 'Miss', 'Ex consectetur vel quia dolorum officiis. Fugit eveniet recusandae repellat. Quia tenetur dolor rem laborum deleniti.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Pipelaying Fitter'),
(60, 'Dr.', 'Omnis dicta expedita rerum possimus. Sed est omnis necessitatibus. Qui expedita quam ea debitis voluptatem aliquid exercitationem et. Nemo ipsum dolorem fugit molestiae. Unde sed rem esse molestias.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Educational Counselor OR Vocationall Counselor'),
(61, 'Mr.', 'Voluptatem aut a laborum quasi vitae deleniti. Illo non qui dolor repellat eos provident. Et optio quis qui autem nihil. Vel voluptatibus ducimus quia culpa tempora nemo.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Compacting Machine Operator'),
(62, 'Mr.', 'Facilis eligendi exercitationem quisquam voluptatibus dignissimos illo in. Sit incidunt pariatur qui odio vitae. Modi natus quia dolorum ut eum et.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Welder'),
(63, 'Prof.', 'Neque nulla dolorem non fugit quia minima omnis id. Sit corporis quos rerum odit et eveniet qui. Quidem nulla sit aperiam sunt.', '2020-08-26 20:17:34', '2020-08-26 20:17:34', NULL, 'Equal Opportunity Representative'),
(64, 'Prof.', 'Voluptas autem itaque molestiae ratione. Sit odit non illum. Explicabo eligendi facere aut voluptate molestias. Sed eaque aut earum libero at consequatur iusto.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Telephone Station Installer and Repairer'),
(65, 'Mrs.', 'Modi error vel provident saepe. Deleniti earum consequatur doloribus tempora officia quod.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Broadcast Technician'),
(66, 'Mrs.', 'Aperiam delectus odio asperiores facere. Nihil sunt nulla deserunt iusto voluptas velit officia. Voluptas est quis incidunt et incidunt. Suscipit aliquam molestiae impedit.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Special Forces Officer'),
(67, 'Prof.', 'Itaque iure ut qui. Quia ratione veniam suscipit eligendi quia animi quae non. Itaque maxime blanditiis quaerat voluptatibus doloribus sunt. Sunt sit doloribus omnis.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Elementary School Teacher'),
(68, 'Prof.', 'Perspiciatis deserunt voluptatem distinctio saepe quasi aliquid. Eum similique quia est sit.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Internist'),
(69, 'Ms.', 'In rem consequuntur molestias quis qui voluptatibus. Esse sed voluptas aut sunt est. Nesciunt eum amet ipsa qui aspernatur. Ipsa error ea illum dolorum.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'CFO'),
(70, 'Dr.', 'Suscipit est assumenda aut minima itaque. Odit qui dolor ea.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Taxi Drivers and Chauffeur'),
(71, 'Dr.', 'Rerum maiores rerum dolorem quos. Iure vel modi recusandae est commodi optio voluptates. Libero ea nesciunt non rerum corporis nam voluptatem dolores. Et et dignissimos possimus architecto similique temporibus consequuntur.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Photographic Restorer'),
(72, 'Prof.', 'Ad qui dolorum odit ut. Et voluptatem molestiae aut sit ut et alias. Tempora aut adipisci blanditiis quae et dignissimos. Dolor sit est ipsum distinctio enim.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Accountant'),
(73, 'Mr.', 'Et dolorum voluptates quo maiores quasi. Quia ratione sequi et repellendus enim explicabo voluptate. Iste enim consequatur saepe aut dolores. Cum numquam rerum assumenda ducimus. Qui tempore vero ea voluptatem iure quibusdam.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Furniture Finisher'),
(74, 'Mr.', 'Sit dolor vero quis facilis ut laborum. Repellendus voluptate corrupti sequi deleniti molestiae autem. Nulla aut occaecati rerum et consequatur.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Photographer'),
(75, 'Mrs.', 'Consequuntur autem debitis sunt quia aliquam dicta aliquid ipsam. Non maiores rerum et quo qui. Et a sapiente dicta rerum doloremque non sunt. Dolorum quo id laudantium reiciendis quae inventore.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Public Health Social Worker'),
(76, 'Dr.', 'Et fuga consequuntur ipsam ea debitis. Vel ad numquam delectus quibusdam quia. Rerum aut vel autem ut doloribus eius id.', '2020-08-26 20:17:35', '2020-08-26 20:17:35', NULL, 'Supervisor of Customer Service'),
(77, 'Miss', 'Animi sunt adipisci nam. Sint enim vero sequi debitis quam labore. Rerum iste tenetur autem vitae maiores neque dolores. Enim temporibus unde nisi et repellat quibusdam.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Platemaker'),
(78, 'Dr.', 'Aut quia voluptatem consequatur temporibus voluptas reprehenderit. Explicabo autem iure atque quo. Laboriosam nihil nostrum dolores molestias fuga necessitatibus.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Gas Appliance Repairer'),
(79, 'Dr.', 'Quia maiores rerum nostrum non delectus eius aut. Minima dolore aperiam sed. Distinctio suscipit et quidem neque voluptatibus officia ut animi.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Geologist'),
(80, 'Dr.', 'Fugiat fugiat et nobis vel eos quisquam necessitatibus. Voluptatem vel consequatur quisquam nesciunt. Et aspernatur rerum laborum tempore. Est nisi laudantium dolor non debitis qui. Dicta nihil maxime quae eum.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Home Appliance Repairer'),
(81, 'Prof.', 'Sunt corporis rerum ipsum. Vel quo sunt molestias quia aut doloremque. Aut possimus corrupti assumenda rem. Animi dolorum in ut sapiente sapiente sed porro. Fuga nulla odio et mollitia ut.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Singer'),
(82, 'Prof.', 'Beatae repudiandae quis dolore ex. Quis esse quasi qui impedit sit debitis. Omnis modi qui occaecati aliquid repudiandae ullam perferendis sit. Quod similique maiores aut porro nostrum.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Health Technologist'),
(83, 'Dr.', 'Ad voluptas architecto sit modi ea enim omnis. Corporis sed et asperiores consectetur. Beatae assumenda quia voluptates error qui beatae. Pariatur assumenda commodi fuga vitae voluptates voluptas molestias.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Public Transportation Inspector'),
(84, 'Prof.', 'Architecto quisquam eos aut fugiat mollitia et. Qui vitae quibusdam vel eligendi aut hic.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Control Valve Installer'),
(85, 'Prof.', 'Itaque voluptatem et ut odit id corporis quis. Sunt autem nobis vel impedit voluptatem minus. Tempore fugiat nostrum aperiam dicta. Aut et voluptas reiciendis. Perferendis accusantium delectus ut ipsam autem laboriosam at aliquid.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Logging Equipment Operator'),
(86, 'Ms.', 'Natus et doloribus non iusto ea omnis eum. Fuga dolorum non quibusdam et. Accusantium voluptatum nihil est tempore dolorem. Voluptas fugiat laborum excepturi facilis.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Etcher and Engraver'),
(87, 'Dr.', 'Et nihil et quia. Quibusdam quisquam suscipit possimus quasi nihil tempore. Quo repudiandae amet error est cum.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Stone Cutter'),
(88, 'Dr.', 'Nobis sunt vero sed deleniti qui repellat nihil debitis. Et qui distinctio eligendi velit consectetur. Rerum eos voluptatum necessitatibus optio qui quisquam. Et sed porro nobis quae.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Computer Security Specialist'),
(89, 'Prof.', 'Eveniet voluptates aliquam quaerat omnis repudiandae. Id sequi perspiciatis placeat a qui autem. Exercitationem ad eum odit in minus. Ut nesciunt ea esse accusamus perferendis rerum aut sit.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Stonemason'),
(90, 'Prof.', 'Cum ipsam iure sed illum. Nisi quisquam culpa enim rerum dignissimos. Molestiae tempora doloremque sint.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Epidemiologist'),
(91, 'Mrs.', 'Atque nam facilis id accusamus voluptas. Velit molestiae hic ea. Sequi nobis ab qui officiis accusantium dolor exercitationem. Et earum quis ab expedita et tenetur et tempora.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Credit Authorizer'),
(92, 'Prof.', 'Sint consequatur cupiditate quibusdam et. Rerum et nostrum ipsam quia eos maxime voluptas. Autem laborum aspernatur enim at quas in enim provident.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Interior Designer'),
(93, 'Dr.', 'Id voluptates vero voluptatem dignissimos. Expedita sunt quaerat occaecati velit ducimus sed nihil nulla. Nostrum in quos veniam consectetur labore inventore consectetur. At natus eligendi labore laborum ratione nesciunt.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Operating Engineer'),
(94, 'Mr.', 'Ut velit inventore molestiae sed quia dolores eos consectetur. Optio nulla suscipit provident ea et inventore. Laboriosam et repellendus facere in consequatur possimus. Omnis qui est magni quia.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Physical Scientist'),
(95, 'Prof.', 'Non est fuga amet quo quia aut. Voluptatum ad eligendi placeat voluptatibus ullam voluptatem fugiat. Deserunt repellat eum distinctio qui est ut iusto. Ea non odit itaque laboriosam dolorem in facilis enim.', '2020-08-26 20:17:36', '2020-08-26 20:17:36', NULL, 'Engineering Technician'),
(96, 'Miss', 'Ut excepturi aut sed hic qui. Deserunt libero vel aspernatur dolorem autem natus et. Esse et eaque similique natus et aut.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Sales Person'),
(97, 'Dr.', 'Laborum qui minus sunt. Vel quod distinctio pariatur aut et. Ducimus suscipit velit adipisci architecto omnis. Est qui quasi inventore perspiciatis aliquam quia.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Commercial Pilot'),
(98, 'Ms.', 'Aut quo qui inventore odio sed. Omnis incidunt cum reprehenderit sunt delectus sed. Incidunt aspernatur ad dolores pariatur dolores. Praesentium eos autem minus ab veritatis enim.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Stock Clerk'),
(99, 'Mr.', 'Quidem dolorum omnis enim. Libero similique sed omnis ipsa. Optio molestiae aut nulla rerum. In ab unde totam odio eum.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Automotive Technician'),
(100, 'Miss', 'Non nobis optio nam aut accusantium dolore quisquam. Iure laudantium dolorem velit dolorem ipsam dignissimos rerum. Id accusantium ex sunt.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Manufactured Building Installer'),
(101, 'Ms.', 'Facilis ea nobis possimus. Dolores inventore odit deleniti est et recusandae numquam. Quia officia aut ut eaque.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Pediatricians'),
(102, 'Dr.', 'Repellendus deleniti sed illo occaecati sapiente consequatur perferendis. Nihil quas nihil in reprehenderit ut tempora est. Ipsum fugit est dolorem deleniti porro sed. Suscipit consequatur distinctio dolores et illum.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Pump Operators'),
(103, 'Ms.', 'Quia exercitationem est aut numquam sit quidem nobis. Assumenda unde est deleniti autem consequuntur. Officiis officia similique delectus et dolores doloremque. Quod aut dolores fugit sequi error fugit amet.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Computer Science Teacher'),
(104, 'Ms.', 'Dolores quidem modi quis quia. Repellendus et dicta sed nihil. Et illum rerum voluptatem repellendus. Excepturi debitis enim possimus corporis optio.', '2020-08-26 20:17:37', '2020-08-26 20:17:37', NULL, 'Office and Administrative Support Worker');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
